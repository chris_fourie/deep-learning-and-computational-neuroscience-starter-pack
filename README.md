# Deep Learning and Computational Neuroscience

## Papers

[Think: Theory for Africa](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6622465/)*

[A deep learning framework for neuroscience](https://www.nature.com/articles/s41593-019-0520-2)*

[Toward an Integration of Deep Learning and Neuroscience](https://www.frontiersin.org/articles/10.3389/fncom.2016.00094/full)* 

[Deep Neural Networks in Computational Neuroscience](https://oxfordre.com/neuroscience/view/10.1093/acrefore/9780190264086.001.0001/acrefore-9780190264086-e-46)

[Deep Learning for Cognitive Neuroscience](https://arxiv.org/abs/1903.01458)

[Deep Learning and Computational Neuroscience](https://link.springer.com/article/10.1007/s12021-018-9360-6)

[A Shared Vision for Machine Learning in Neurosience](https://www.jneurosci.org/content/38/7/1601.abstract)

## Mailing lists

[Comp-Neuro](http://www.tnb.ua.ac.be/mailman/listinfo/comp-neuro) [[1]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6622465/)

[[Theoretical Neuroscience: Computational and Mathematical Modeling of Neural Systems](https://www.amazon.com/Theoretical-Neuroscience-Computational-Mathematical-Modeling/dp/0262541858) by Peter Dayan and Laurence F. Abbott Connectionists](https://mailman.srv.cs.cmu.edu/mailman/listinfo/connectionists) [[1]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6622465/)

[Machine Learning and Data Science Africa](https://groups.google.com/forum/#!forum/mlds-africa) [[1]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6622465/)

## Text Books

[Theoretical Neuroscience: Computational and Mathematical Modeling of Neural Systems](https://www.amazon.com/Theoretical-Neuroscience-Computational-Mathematical-Modeling/dp/0262541858) by Peter Dayan and Laurence F. Abbott [[1]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6622465/)

[Information Theory, Inference and Learning Algorithms](https://www.amazon.com/Information-Theory-Inference-Learning-Algorithms/dp/0521642981) by David J. C. MacKay [[1]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6622465/)

[Neuronal Dynamics:](https://neuronaldynamics.epfl.ch/)
[From single neurons to networks and models of cognition](https://neuronaldynamics.epfl.ch/) by Wulfram Gerstner, Werner M. Kistler, Richard Naud, and Liam Paninski [[1]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6622465/)

## Summer schools

[IBRO-Simons Computational Neuroscience Imbizo](https://imbizo.africa/) [[1]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6622465/)
Cape Town, South Africa

[Deep Learning Indaba](http://www.deeplearningindaba.com/), Africa, (Location changes annually) [[1]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6622465/)

[Methods in Computational Neuroscience](https://www.mbl.edu/mcn/), Woods Hole, US [[1]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6622465/)

## Conferences

[Computational and Systems Neuroscience (COSYNE)](http://www.cosyne.org/) [[1]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6622465/)*

[Neural Coding, Computation and Dynamics](http://www.gatsby.ucl.ac.uk/nccd/) (NCCD) [[1]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6622465/)

[Bernstein Conference](https://www.bernstein-network.de/en/bernstein-conference) [[1]](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6622465/)

## Courses 

Coursera - [Computational Neuroscience](https://www.coursera.org/learn/computational-neuroscience)

## Article

[A Brief Introduction to Computational Neuroscience](https://towardsdatascience.com/a-brief-introduction-to-computational-neuroscience-part-1-42171791f613)

## Discussions

[Reddit](https://www.reddit.com/r/MachineLearning/comments/9pmqya/d_computational_neuroscience_and_machine_learning/)

## References

[[1] Think: Theory for Africa](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6622465/)